library ieee;
use ieee.std_logic_1164.all;
use STD.TEXTIO.all;
use work.float_pkg_cust.all;

package fpa_test_vect is

constant HIGHZ : std_logic_vector (31 downto 0)
           := "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";

procedure gen_vec (SIGNAL aid_sig,bid_sig,resid_sig : OUT string (1 to 6);
               SIGNAL aval,bval,exp_res : OUT std_logic_vector (31 downto 0);
               SIGNAL C : IN std_logic_vector (31 downto 0);
               SIGNAL latch,drive : OUT std_ulogic;
               SIGNAL err_sig : OUT bit;
               SIGNAL score : OUT integer);

end fpa_test_vect;

package body fpa_test_vect is

procedure gen_vec (SIGNAL aid_sig,bid_sig,resid_sig : OUT string (1 to 6);
               SIGNAL aval,bval,exp_res : OUT std_logic_vector (31 downto 0);
               SIGNAL C : IN std_logic_vector (31 downto 0);
               SIGNAL latch,drive : OUT std_ulogic;
               SIGNAL err_sig : OUT bit;
               SIGNAL score : OUT integer) is
variable cur_line : LINE;
file test_data: TEXT is IN "/user2/faculty/degroat/ee762_assign/fpavectors";
variable a_test_val, b_test_val : bit_vector (31 downto 0);
variable result_val : bit_vector (31 downto 0);
variable std_result_val : std_logic_vector (31 downto 0);
variable aid,bid,resid : string (1 to 6);
variable num_tests,num_errors : integer := 0;
variable exp_float, a_float, b_float : float32 := "00000000000000000000000000000000";
Begin
  aval <= HIGHZ; bval <= HIGHZ; exp_res <= HIGHZ;
  latch <= '1'; drive <= '1';
  WHILE (NOT ENDFILE(test_data)) LOOP
    --get next input test vector and expected result
    readline(test_data,cur_line);
    read(cur_line,aid); read(cur_line,a_test_val);
    read(cur_line,bid); read(cur_line,b_test_val);
    readline(test_data,cur_line);
    num_tests := num_tests + 1;
    -- run through bus cycle to send data to unit
    aid_sig <= "======", aid after 20 ns; 
    bid_sig <= "======", bid after 20 ns; 
    resid_sig <= "======", resid after 20 ns;
    -- drive signals on bus
    aval <= To_StdLogicVector(a_test_val) after 20 ns, HIGHZ after 80 ns;
    bval <= To_StdLogicVector(b_test_val) after 20 ns, HIGHZ after 80 ns;
    latch <= '0' after 20 ns, '1' after 70 ns;
    wait for 100 ns;
    drive <= '0' after 20 ns, '1' after 80 ns;
    wait for 50 ns;
    a_float := to_float(aval);
    b_float := to_float(bval);
    exp_res <= to_slv(a_float + b_float) after 20 ns, HIGHZ after 80 ns;
    ASSERT (C = exp_res)
       REPORT "result does not agree with expected float result"
       SEVERITY WARNING;
    IF (C /= exp_res) THEN
       num_errors := num_errors + 1; 
       err_sig <= '1', '0' after 10 ns;
    END IF;
    wait for 50 ns;
  END LOOP;
  score <= ((num_tests - num_errors)* 100)/num_tests;
  wait for 100 ns;
  wait;
END gen_vec;

end fpa_test_vect;

