library IEEE;
use ieee.std_logic_1164.all;
package fpa_support is

  constant HIGHZ : std_logic_vector (31 downto 0)
           := "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
  constant NAN : std_logic_vector (31 downto 0)
           := "01111111100000000000000000000001";
  constant man_zero : std_logic_vector (23 downto 0)
           := "000000000000000000000000";
  constant exp_zero : std_logic_vector (7 downto 0)
           := "00000000";
  constant exp_ones : std_logic_vector (7 downto 0)
           := "11111111";
  constant const_one : std_logic_vector (24 downto 0)
           := "0000000000000000000000001";

  function std8_2_int (A : std_logic_vector(7 downto 0))
                               return integer;

  function int_2_std8 (A : integer) return std_logic_vector;

  procedure normalize (res_exp_int : IN integer;
                       add_res : IN std_logic_vector (24 downto 0);
                       rexp : OUT std_logic_vector (7 downto 0);
                       rman : OUT std_logic_vector (23 downto 0));

  procedure std_logic_add (a   : IN std_logic_vector;
                            sum : INOUT std_logic_vector);

end fpa_support;

package body fpa_support is

  function std8_2_int (A : std_logic_vector (7 downto 0))
                               return integer is
    variable wt,int_val : integer;
  BEGIN
    wt := 1;
    int_val := 0;
    for i in 0 to 7 loop
      if (A(i) = '1' or A(i) = 'H') 
         THEN int_val := int_val + wt;
      end if;
      wt := wt * 2;
    end loop;
    RETURN int_val;
  END std8_2_int;

  function int_2_std8 (A : integer) return std_logic_vector is
    variable wt,cur_val : integer;
    variable std_val : std_logic_vector(7 downto 0);
  BEGIN
    cur_val := A;
    wt := 128;
    std_val := exp_zero;
    for i in 7 downto 0 loop
      if (cur_val >= wt) THEN
          cur_val := cur_val - wt;
          std_val(i) := '1';
      end if;
      wt := wt/2;
    end loop;
    RETURN std_val;
  END int_2_std8;

  procedure  std_logic_add (a   : IN std_logic_vector;
                            sum : INOUT std_logic_vector) is
     variable carry_in, carry_out : std_logic;
     variable new_sum : std_logic_vector(sum'RANGE);
  BEGIN
     carry_out := '0';
         for i in sum'LOW to sum'HIGH loop
                carry_in := carry_out;
        new_sum(i) := a(i) xor sum(i) xor carry_in;
                carry_out := (a(i) and sum(i)) or (a(i) and carry_in) 
                        or (sum(i) and carry_in);
     end loop;
     sum := new_sum; 
  END std_logic_add;

  procedure normalize (res_exp_int : IN integer;
                       add_res : IN std_logic_vector(24 downto 0);
                       rexp : OUT std_logic_vector(7 downto 0);
                       rman : OUT std_logic_vector(23 downto 0)) is
    variable exp_val : integer;
    variable man_val : std_logic_vector(24 downto 0);
  BEGIN
    exp_val := res_exp_int;
    man_val := add_res;
    -- adjust mantissa until a leading 1.xxx
    IF (man_val(24) = '1')  THEN
        exp_val := exp_val + 1;
        man_val := '0' & man_val(24 downto 1);
    ELSE
        while (man_val(23) = '0' and man_val(23 downto 0)/=man_zero) loop
           exp_val := exp_val -1;
           man_val := man_val(23 downto 0) & '0';
        end loop;
    END IF;
    -- generate normalized result
    IF (exp_val > 254) THEN
        rexp := exp_ones;
        rman := man_zero;
    ELSIF (exp_val > 0) THEN
        rexp := int_2_std8(exp_val);
        rman := man_val(23 downto 0);
    ELSE
        while (exp_val < 0) LOOP
           exp_val := exp_val + 1;
           man_val := '0' & man_val(24 downto 1);
        END LOOP;
        rexp := exp_zero;
        rman := man_val(23 downto 0);
    END IF;       
  END normalize;

end fpa_support;

